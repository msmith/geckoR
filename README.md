CRISPR Package Development
================================

Developmental version of a R package for analysing data from CRISPR screens.

## R library installation

Before installing the R library from here you need to manually install the libraries it depends on.  Usually this is taken care of by R when software is in Bioconductor, but not for us at the moment.  Running the following commands in R before you install the package should be sufficient.  This only needs to be done once.

```
source("http://www.bioconductor.org/biocLite.R")
biocLite(c("dplyr","ggplot2","data.table","lmerTest","Biostrings",
          "Rsubread","ShortRead","gridExtra","tibble","tidyr",
          "tools","optimx","stringr","knitr","hadley/multidplyr"))
```
